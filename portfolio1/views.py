from django.shortcuts import render

# Create your views here.
response  = {}
def index(request):
	response['Author'] = "Muhammad Arief Wibowo"
	html = "layout/base.html"
	return render(request, html, response)
